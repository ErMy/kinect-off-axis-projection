# Kinect Off Axis Projection

Uses off-axis-cameras combined with head tracking (using Kinect) to create a 3D effect when looking at a display. Works for displays of any size, aspect ratio and with multiple displays.

<img src="https://i.imgur.com/oMfkHKe.mp4" title="source: imgur.com"/>
